package phonebook;

public class Contact {

    private final String firstname;
    private final String lastName;
    private final String phoneNumber;
    public String additionalInfo;

    public Contact(String firstname, String lastName, String phoneNumber, String additionalInfo) {
        this.firstname = firstname;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.additionalInfo = additionalInfo;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @Override
    public String toString() {
        return firstname +
                ", " + lastName +
                ", " + phoneNumber +
                ", " + additionalInfo;
    }
}
