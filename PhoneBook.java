package phonebook;

import javax.swing.*;
import java.io.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class PhoneBook {

    private ArrayList<Contact> contact = new ArrayList<>();
    String filepath = "contacts.txt";
    File myObj = new File(filepath);

    public String addContact() {
        String firstName = JOptionPane.showInputDialog("Enter First Name");
        String lastName = JOptionPane.showInputDialog("Enter Last Name");
        String phoneNumber = JOptionPane.showInputDialog("Enter phone number");
        String additionalInfo = JOptionPane.showInputDialog("Enter additional information");

        String[] availableCategories = {"Text", "Numbers"};

        String categories = (String) JOptionPane.showInputDialog(
                null,
                "Select a category",
                "Contact category",
                JOptionPane.QUESTION_MESSAGE,
                null,
                availableCategories,
                availableCategories[0]
        );

        Contact contact = new Contact(firstName, lastName, phoneNumber, additionalInfo);
        try {
            FileWriter myWriter = new FileWriter(filepath);
            myWriter.write(String.valueOf(contact));
            myWriter.close();
            System.out.println("Successfully wrote to the file");
        } catch (IOException e) {
            System.out.println("An error has occurred");
            e.printStackTrace();
        }
        this.contact.add(contact);
        return contact + " was added successfully";
    }

    public ArrayList<Contact> seeContact() {
        return contact;
    }

    public String searchContact(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the last name you are looking for");
        String lastName = scanner.nextLine();
        try {
            scanner = new Scanner(myObj).useDelimiter(",");

            while (scanner.hasNext()) {
                final String lineFromFile = scanner.nextLine();
                if (lineFromFile.contains(lastName)) {
                    System.out.println("The last name " + lastName + " was found");
                    break;
                } else {
                    System.out.println("The last name " + lastName + " does not exist");
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("An error has occurred");
        }
        scanner.close();
        return lastName;
    }

    public String removeContact() {
        String firstName = JOptionPane.showInputDialog("Enter First Name");
        String lastName = JOptionPane.showInputDialog("Enter Last Name");
        String phoneNumber = JOptionPane.showInputDialog("Enter phone number");
        String additionalInfo = JOptionPane.showInputDialog("Enter additional information");

        String[] availableCategories = {"Text", "Numbers"};

        String categories = (String) JOptionPane.showInputDialog(
                null,
                "Select a category",
                "Contact category",
                JOptionPane.QUESTION_MESSAGE,
                null,
                availableCategories,
                availableCategories[0]
        );

        Contact contact = new Contact(firstName, lastName, phoneNumber, additionalInfo);
        try {
            File inputFile = new File(filepath);
            File tempFile = new File("myTempFile.txt");

            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

            String lineToRemove = String.valueOf(contact);
            System.out.println(lineToRemove);
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                String trimmedLine = currentLine.trim();
                if (trimmedLine.equals(lineToRemove)) continue;
                writer.write(currentLine + System.getProperty(","));
            }
            writer.close();
            reader.close();
            boolean delete = inputFile.delete();
            boolean is_success = tempFile.renameTo(inputFile);
        } catch (IOException e) {
            System.out.println("An error has occurred");
        }
        return contact + " removed successfully";
    }
}
